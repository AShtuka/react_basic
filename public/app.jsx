class Profile extends React.Component {

    constructor(props) {
        super(props)
        // console.log(1, 'constructor')
        console.clear()
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log(2, 'should component update')
        return true
    }

    // componentWillMount(){
    //     console.log(2, 'UNSAFE_componentWillMount')
    // }

    componentWillUpdate(nextProps, nextState) {
        console.log(3, 'UNSAFE_componentWillUpdate')
    }

    componentWillReceiveProps(nextProps) {
        console.log(1, 'UNSAFE_componentWillReceiveProps')
    }

    componentDidUpdate(prevProps, prevState){
        console.log(5, 'componentDidUpdate')
    }

    // componentDidMount(){
    //     console.log(4, 'componentDidMount')
    // }

    componentWillUnmount(){
        console.log('componentWillUnmount')
    }

    render () {
        // console.log(3, 'first render')
        console.log(4, 'render')
        return(
            <div className='profile'>
                <img src="http://i.pravatar.cc/300" alt="IMG NOT FOUND"/>
                <h1>{this.props.name} {this.props.surname}</h1>
                <p>Age: {this.props.age}</p>
                <p>Sex: {this.props.gender}</p>
                <p>About me: {this.props.about}</p>
            </div>
        )
    }
}

class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            age : 25
        }
    }

    // state = {
    //     age : 25
    // }

    newRandomAge = () => {
        this.setState({
            age : Math.ceil(Math.random() * 100)
        })
    }

    render(){
        return(
            <div>
                <button onClick={this.newRandomAge}>Rerender</button>
                {this.state.age < 50 ? (
                    <Profile
                        name='Nicky'
                        surname='Black'
                        age={this.state.age}
                        gender='male'
                        about='something about me...'
                    />) : (null)
                }
            </div>
        )
    }
}

function HeadingItem(props) {
    const Heading = props.tagType
    return <Heading>{props.headingText}</Heading>
}

function DescriptionItem(props) {
    const Desc = props.tagType
    return <Desc>{props.descriptionText}</Desc>
}

function SectionItem (props) {
    return (
        <section className='item'>
            <HeadingItem
                headingText={props.headingText}
                tagType={props.headingTagType}
            />

            <DescriptionItem
                descriptionText={props.descriptionText}
                tagType={props.descriptionTagType}
            />
        </section>
    )
};

ReactDOM.render(
    <div>
        <div className='wrapper'>
            <SectionItem
                headingText='HTML'
                headingTagType='h1'
                descriptionText='Transform into the DOM'
                descriptionTagType='p'
            />

            <SectionItem
                headingText='CSS'
                headingTagType='h2'
                descriptionText='Style DOM-element'
                descriptionTagType='quote'
            />

            <SectionItem
                headingText='JavaScript'
                headingTagType='h3'
                descriptionText='Adds interactivity to elements'
                descriptionTagType='div'
            />
        </div>
        <App />
    </div>,
    document.getElementById('app')
);