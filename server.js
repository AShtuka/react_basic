const express = require('express');
const app  = express();
app.use(express.static('public'));
app.use(function log (req, res, next) {
    console.log([req.method, req.url].join(' '));
    next();
});
app.listen(3333, function () {
    console.log('Listening on port 3333!');
})